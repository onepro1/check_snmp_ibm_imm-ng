#!/bin/sh

#
# This is a script for monitoring sensors (temperature, fans and voltage) and
# overall health of IBM servers using SNMPv1 to the Integrated Management Module (IMM).
#
#Version 0.4 2019-10-04
#Sector Nord AG 
#added SNMPv3 support
#added check for disk, RAID, volume
#reduce count of functions (three functions for string operations instead one function for every check)

# Version 0.4 2017-10-11
#Added a 5 second timeout to snmpwalk instead of the default of one.
# Fixed a bug with the exit status for help.

# Version 0.3 2017-10-09
# Farid Joubbi farid@joubbi.se
# Added warning and critical level to temperature performance data.
# Added label for performance data instead of just a number.
# Added check for 0 in critical temperature.
# Removed ofmaximum from fan speed output for nicer performance graphs.
# Renamed offline to zero in performance data for fans.


# Version 0.0.2 2010-08-24
# Return 3 for unknown results.

# Version 0.0.1 2010-05-21
# Ulric Eriksson <ulric.eriksson@dgc.se>

#Sector Nord AG 2019-09-19
#added SNMPv3 support
#added check for disk status and RAID status
#removed a lot of functions

SNMPWALK="/usr/bin/snmpwalk -On"

BASEOID=.1.3.6.1.4
IMMOID=$BASEOID.1.2.3.51.3

tempOID=$IMMOID.1.1
tempsOID=$tempOID.1.0
# Temperature sensor count
tempIndexOID=$tempOID.2.1.1
# Temperature sensor indexes
tempNameOID=$tempOID.2.1.2
# Names of temperature sensors
tempTempOID=$tempOID.2.1.3
tempFatalOID=$tempOID.2.1.5
tempCriticalOID=$tempOID.2.1.6
tempNoncriticalOID=$tempOID.2.1.7

voltOID=$IMMOID.1.2
voltsOID=$voltOID.1.0
voltIndexOID=$voltOID.2.1.1
voltNameOID=$voltOID.2.1.2
voltVoltOID=$voltOID.2.1.3
voltCritHighOID=$voltOID.2.1.6
voltCritLowOID=$voltOID.2.1.7

fanOID=$IMMOID.1.3
fansOID=$fanOID.1.0
fanIndexOID=$fanOID.2.1.1
fanNameOID=$fanOID.2.1.2
fanSpeedOID=$fanOID.2.1.3
fanMaxSpeedOID=$fanOID.2.1.8

diskOID=$IMMOID.1.12
diskNumber=$diskOID.1.0
diskIndexOI=$diskOID.2.1.1
diskNROID=$diskOID.1.0
diskNameOID=$diskOID.2.1.2
diskStatusOID=$diskOID.2.1.3

raidOID=$IMMOID.1.13.1
raidStoragePoolTable=$raidOID.6
raidStoragePoolIndex=$raidStoragePoolTable.1.1
raidStoragePoolName=$raidStoragePoolTable.1.2
raidStoragePoolState=$raidStoragePoolTable.1.4
raidStoragePoolDrives=$raidStoragePoolTable.1.7
raidVolTable=$raidOID.7
raidVolIndex=$raidVolTable.1.1
raidVolName=$raidVolTable.1.2
raidVolStatus=$raidVolTable.1.4
raidDriveTable=$raidOID.3
raidDriveEntry=$raidDriveTable.1
raidDriveIndex=$raidDriveTable.1.1
raidDriveName=$raidDriveTable.1.2
raidDriveSlotNo=$raidDriveTable.1.5
raidDriveState=$raidDriveTable.1.4
raidDriveCurTemp=$raidDriveTable.1.10
raidDriveSlotNo=$raidDriveTable.1.5
raidDriveVPDSerialNo=$raidDriveTable.1.17


healthStatOID=$IMMOID.1.4.1.0
# 255 = Normal, 0 = Critical, 2 = Non-critical Error, 4 = System-level Error

# 'label'=value[UOM];[warn];[crit];[min];[max]

usage()
{
	echo "Usage: $0 -H HOST -u SNMP_USERNAME -v SNMP_APROTOCOL -A SNMP_Password -a SHA -X UserPassword2 -x AES -l authPriv -T health|temperature|voltage|fans|raid|disks|raid|volume"
}

get_health()
{
	echo "$SEARCH"|grep "^$1."|head -1|sed -e 's,^.*: ,,'|tr -d '"'
}

get_string()
{
        # $2= passed OID, $1=result from $raid, for example number 1-4. When assembled, the correct OID is obtained for the information queried.
        echo "$SEARCH"|grep "^$2.*$1 = "|head -1 |sed -e 's,^.*: ,,'|tr -d '"'
}
get_string_wo_linebreaks()
{
        #removes blanks und linebreaks
        echo "$SEARCH"|grep "^$2.*$1 = "|head -1 |sed -e 's,^.*: ,,'|tr -d '"'| tr -d '[:blank:]'
}
get_string_remove_characters_after_whitespaces()
{
        #removes characters after whitespaces. Maybe necessary for temperature of disks (23 C >> 23 for performance data)
        echo "$SEARCH"|grep "^$2.*$1 = "|head -1 |sed -e 's,^.*: ,,'|sed -e 's/\s.*$//'|tr -d '"'
}

if test "$1" = -h; then
	usage
        exit 0
fi

while getopts "H:u:v:A:a:x:X:T:l:" o; do
	case "$o" in
	H )
		HOST="$OPTARG"
		;;
	u )
		USER="$OPTARG"
		;;
	v )
		VERSION="$OPTARG"
		;;
	A )
		PASSWORD="$OPTARG"
		;;
	a )
		SNMP_APROTOCOL="$OPTARG"
		;;
	X )
		SNMP_PPASSWORD="$OPTARG"
		;;
	x )
		SNMP_PPROTOCOL="$OPTARG"
		;;
	l )
		AUTHPRIV="$OPTARG"
		;;
	T )
		TEST="$OPTARG"
		;;
	* )
		usage
                exit 3
		;;
	esac
done

#SNMPOPTS=" -v 1 -c $COMMUNITY -On -t 5 $HOST"

SNMPOPTS="-v $VERSION -l $AUTHPRIV -u $USER -A $PASSWORD -a SHA -X $SNMP_PPASSWORD -x $SNMP_PPROTOCOL -t 5 $HOST"

RESULT=''
STATUS=0	# OK

case "$TEST" in
health )
	SEARCH=`$SNMPWALK $SNMPOPTS $healthStatOID`
	healthStat=`get_health $healthStatOID`
	case "$healthStat" in
	0 )
		RESULT="Health status: Critical"
		STATUS=2	# Critical
		;;
	2 )
		RESULT="Health status: Non-critical error"
		STATUS=1
		;;
	4 )
		RESULT="Health status: System level error"
		STATUS=2
		;;
	255 )
		RESULT="Health status: Normal"
                STATUS=0
		;;
	* )
		RESULT="Health status: Unknown"
		STATUS=3
		;;
	esac
	;;
temperature )
	SEARCH=`$SNMPWALK $SNMPOPTS $tempOID`
	# Figure out which temperature indexes we have
	temps=`echo "$SEARCH"|
	grep -F "$tempIndexOID."|
	sed -e 's,^.*: ,,'`
	#echo $temps
	if test -z "$temps"; then
		RESULT="No temperatures"
		STATUS=3
	fi
	for i in $temps; do
	#echo $i
		tempName=`get_string $i $tempNameOID`
		tempTemp=`get_string $i $tempTempOID`
		tempFatal=`get_string $i $tempFatalOID`
		tempCritical=`get_string $i $tempCriticalOID`
		tempNoncritical=`get_string $i $tempNoncriticalOID`
		RESULT="$RESULT$tempName = $tempTemp"
		if test "$tempCritical" -gt 0; then
			if test "$tempTemp" -ge "$tempCritical"; then
				STATUS=2
			elif test "$tempTemp" -ge "$tempNoncritical"; then
				STATUS=1
			fi
		fi
		PERFDATA="${PERFDATA}'$tempName'=$tempTemp;$tempNoncritical;$tempCritical;; "
	done
	;;
voltage )
	SEARCH=`$SNMPWALK $SNMPOPTS $voltOID`
	volts=`echo "$SEARCH"|
	grep -F "$voltIndexOID."|
	sed -e 's,^.*: ,,'`
	if test -z "$volts"; then
		RESULT="No voltages"
		STATUS=3
	fi
	for i in $volts; do
		voltName=`get_string $i $voltNameOID`
		voltVolt=`get_string $i $voltVoltOID`
		voltCritHigh=`get_string $i $voltCritHighOID`
		voltCritLow=`get_string $i $voltCritLowOID`
		RESULT="$RESULT$voltName = $voltVolt"
		if test "$voltCritLow" -gt 0 -a "$voltVolt" -le "$voltCritLow"; then
			#echo "$voltVolt < $voltCritLow"
			STATUS=2
		elif test "$voltCritHigh" -gt 0 -a "$voltVolt" -ge "$voltCritHigh"; then
			#echo "$voltVolt > $voltCritLow"
			STATUS=2
		fi
		PERFDATA="${PERFDATA}'$voltName'=$voltVolt;;;; "
	done
	;;
fans )
	SEARCH=`$SNMPWALK $SNMPOPTS $fanOID`
	fans=`echo "$SEARCH"|
	grep -F "$fanIndexOID."|
	sed -e 's,^.*: ,,'`
	if test -z "$fans"; then
		RESULT="No fans"
		STATUS=3
	fi
	for i in $fans; do
		fanName=`get_string $i $fanNameOID`
		fanSpeed=`get_string $i $fanSpeedOID|tr -d 'h '| sed -e 's/ofmaximum//g'`
		RESULT="$RESULT$fanName = $fanSpeed"
		fanSpeedPerf=`echo $fanSpeed | sed -e 's/offline/0/g'`
		PERFDATA="${PERFDATA}'$fanName'=$fanSpeedPerf;;;; "
	done
	;;
disks )
    SEARCH=`$SNMPWALK $SNMPOPTS $diskOID`
	disks=`echo "$SEARCH"|
    grep -F "$diskIndexOI"|
    sed -e 's,^.*: ,,'`
    if test -z "$disks"; then
                RESULT="No Disks"
                STATUS=2
    fi
	for i in $disks; do
		diskstatus=`get_string_wo_linebreaks $i $diskStatusOID`	
		diskname=`get_string_wo_linebreaks $i $diskNameOID`
			if  [ "$diskstatus" = "Normal" ]; then
			RESULT="All Disks: $diskstatus"
			STATUS=0
			else
			RESULTLOOP="$diskname:$diskstatus"
			STATUS=2
			fi
			RESULT=$RESULTLOOP$RESULT
		done
		diskcount=`get_string $diskNumber`
		PERFDATA="${PERFDATA}'Disks'=$diskcount;;;; "
	;;
raid )
	SEARCH=`$SNMPWALK $SNMPOPTS $raidVolTable`
    raids=`echo "$SEARCH"|
    grep -F "$raidVolIndex"|
    sed -e 's,^.*: ,,'`
    if test -z "$SEARCH"; then
        RESULT="No RAID"
        STATUS=3
    fi
	for i in $raids; do
        raidname=`get_string $i $raidVolName`
        raidstatus=`get_string $i $raidVolStatus`
        if [ $raidstatus = "Optimal" ]; then
		        STATUS=0
		        RESULT="$raidname:$raidstatus"
	else
		        STATUS=2
		        RESULT="$raidname:$raidstatus"
	fi
	done
    ;;
volume )
	SEARCH=`$SNMPWALK $SNMPOPTS $raidDriveTable`
	searchs=`echo "$SEARCH"|
	grep -w "$raidDriveIndex"|
	sed -e 's,^.*: ,,'`
	for i in $searchs; do
		drivetemp=`get_string_remove_characters_after_whitespaces $i $raidDriveCurTemp`
		drivename=`get_string $i $raidDriveName`
		drivestatus=`get_string_wo_linebreaks $i $raidDriveState`
		driveslot=`get_string_wo_linebreaks $i $raidDriveSlotNo`
		driveSN=`get_string_remove_characters_after_whitespaces $i $raidDriveVPDSerialNo`
        if [ $drivestatus = "Online" ]; then
		        STATUS=0
		        RESULT="All drives :$drivestatus"
	    else
		        STATUS=2
		        RESULTLOOP="$drivename: $drivetemp"" Grad Celsius":"Status: $drivestatus":"Slot: $driveslot ":"Seriennummer: $driveSN"
	    fi
		PERFDATA="${PERFDATA}'$drivename'=$drivetemp;;;;"
		RESULT=$RESULTLOOP$RESULT
	done
	;;
* )
	usage
        exit 3
	;;
esac

echo "$RESULT|$PERFDATA"
exit $STATUS